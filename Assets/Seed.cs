﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : MonoBehaviour
{
    public string seedString = "Seed String";
    public bool useSeedString;
    public int seed;
    public bool randomizeSeed;
    
    
    void Awake()
    {
        if (useSeedString)
        {
            seed = seedString.GetHashCode();
        }

        if (randomizeSeed)
        {
            seed = Random.Range(0, 9999);
        }

        Random.InitState(seed);
    }
}
