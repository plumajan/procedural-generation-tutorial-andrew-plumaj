﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrianGenerator : MonoBehaviour
{

    public int terrianSize;
    public float terrianScale;
    public float heightMultiplier;

    public ForestGenerator forest;

    private float[,] heightMap;

    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    Material material;

    private void Start()
    {
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshFilter = gameObject.AddComponent<MeshFilter>();
        material = new Material(Shader.Find("Standard"));


        if(terrianSize < forest.forestSize)
        {
            terrianSize = forest.forestSize;
        }

        heightMap = new float[terrianSize, terrianSize];

        GeneraterHeightMap();
        Generate();
        forest.Generate(heightMap);
    }

    float GetHeight(Vector3 position)
    {
        return Mathf.PerlinNoise((position.x + 0.1f) / terrianSize * terrianScale, (position.z + 0.1f) / terrianSize * terrianScale) * heightMultiplier;
    }

    void GeneraterHeightMap()
    {
        for (int x = 0; x < terrianSize; x++)
        {
            for (int z = 0; z < terrianSize; z++)
            {
                heightMap[x, z] = GetHeight(new Vector3(x, 0f, z));
            }
        }
    }

    void Generate()
    {
        Vector3[] vertices = new Vector3[terrianSize * terrianSize];
        int[] triangles = new int [(terrianSize - 1) * (terrianSize - 1) * 6];
        Vector2[] uvs = new Vector2[terrianSize * terrianSize];

        int vertIndex = 0;
        int triIndex = 0;

        for (int x = 0; x < terrianSize; x++)
        {
            for (int z = 0; z < terrianSize; z++)
            {
                vertices[vertIndex] = new Vector3(x, heightMap[x, z], z);
                uvs[vertIndex] = new Vector2((float)(x / terrianSize), (float)(z / terrianSize));

                if(x < terrianSize - 1 && z < terrianSize - 1)
                {
                    triangles[triIndex] = vertIndex;
                    triangles[triIndex + 1] = vertIndex + terrianSize + 1;
                    triangles[triIndex + 2] = vertIndex + terrianSize;
                    triangles[triIndex + 3] = vertIndex + terrianSize + 1;
                    triangles[triIndex + 4] = vertIndex;
                    triangles[triIndex + 5] = vertIndex + 1;

                    triIndex += 6;
                }

                vertIndex++;
            }
        }

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;

        mesh.RecalculateNormals();
        meshFilter.mesh = mesh;

        Texture2D texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, Color.white);
        texture.Apply();
        material.mainTexture = texture;
        meshRenderer.material = material;
    }
}
